var home = {
    init:function () {
         $('[data-toggle="popover"]').popover({html:true});
    }
}


var login = {
            init : function () {
               $('.js-sign-in-button').click(function() {
                   var username = $('.js-login-username').val();
                   var password = $('.js-login-password').val();
                   var csrftoken = $('.csrfmiddlewaretoken').val()

                    //if(username==null)
                      //  return alert('Kullanıcı adı boş olamaz.')

                   //if(password==null)
                     //   return alert('Sifre adı boş olamaz.')

                   if(username!=""&&password!=""){
                   var data = { 'name' :username, 'password': password}

                   login.sendAjaxPostRequest(data);

                   }
                   else{
                       alert("Kullanıcı adı/ şifre boş bırakılamaz.")
                   }

               });
            },
            sendAjaxPostRequest:function (data) {
                var csrftoken = login.getCookie('csrftoken');

                  $.ajax({
                      type : "POST",
                      dataType: "json",
                      url : "/user/login/",
                      data : {"name":data.name, "password": data.password },
                      headers: {
                          "HTTP_X_CSRF_TOKEN":csrftoken,
                          "X-CSRFToken":csrftoken
                      },
                      success: function(data){
                        if(data.response.success==true){
                           location.reload()
                        }else{
                            alert('Giriş yapılamadı')
                        }
                      },
                      error: function(XMLHttpRequest, textStatus, errorThrown) {
                         alert("some error " + String(errorThrown) + String(textStatus) + String(XMLHttpRequest.responseText));
                      }
                    });
            },

            getCookie : function (cname) {

                    var name = cname + "=";
                    var decodedCookie = decodeURIComponent(document.cookie);
                    var ca = decodedCookie.split(';');
                    for(var i = 0; i <ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') {
                            c = c.substring(1);
                        }
                        if (c.indexOf(name) == 0) {
                            return c.substring(name.length, c.length);
                        }
                    }
                    return "";
                }


 }

 var register = {
    init: function () {
        $('.js-signup-button').click(function () {

            var username = $('.js-register-username').val();
            var password = $('.js-register-password').val();
            var email  = $('.js-register-email').val();

            var userTerms = $('.js-register-userterms').is(':checked');

            var data= {"name":username, "password":password,"email":email, "userTerms":userTerms};


            if(!userTerms)
                return alert("Üyelik sözleşmesini onaylayınız");

            if(username!="" && password != "" && email != ""&& userTerms)
            {
                register.sendAjaxPostRequest(data)
            }
            else
            {
                return alert("Bilgileri eksiksi doldurmanız gerekmektedir.")
            }

        });
    },
     sendAjaxPostRequest:function (data) {
        var csrftoken = login.getCookie('csrftoken');

      $.ajax({
          type : "POST",
          dataType: "json",
          url : "/user/signup/",
          data :
              {
                "name": data.name,
                "password":     data.password,
                "email":    data.email,
                "userterms":    data.userTerms
              },
          headers: {
              "HTTP_X_CSRF_TOKEN":csrftoken,
              "X-CSRFToken":csrftoken
          },
          success: function(data){
            if(data.response.success==true){
               location.reload()
            }else{
                alert('Kayıt olma işlemi başarısız.')
            }
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
             alert("some error " + String(errorThrown) + String(textStatus) + String(XMLHttpRequest.responseText));
          }
        });
}

 }

 var video = {
         init:function () {
        var
                sourcesSelector = document.body.querySelectorAll('select'),
		        sourcesTotal = sourcesSelector.length
            ;

            for (var i = 0; i < sourcesTotal; i++) {
                sourcesSelector[i].addEventListener('change', function (e) {
                    var
                        media = e.target.closest('.media-container').querySelector('.mejs__container').id,
                        player = mejs.players[media]
                    ;

                    player.setSrc(e.target.value.replace('&amp;', '&'));
                    player.setPoster('');
                    player.load();

                });

                // These media types cannot play at all on iOS, so disabling them
                if (mejs.Features.isiOS) {
                    sourcesSelector[i].querySelector('option[value^="rtmp"]').disabled = true;
                    if (sourcesSelector[i].querySelector('option[value$="webm"]')) {
                        sourcesSelector[i].querySelector('option[value$="webm"]').disabled = true;
                    }
                    if (sourcesSelector[i].querySelector('option[value$=".mpd"]')) {
                        sourcesSelector[i].querySelector('option[value$=".mpd"]').disabled = true;
                    }
			        if (sourcesSelector[i].querySelector('option[value$=".ogg"]')) {
                        sourcesSelector[i].querySelector('option[value$=".ogg"]').disabled = true;
                    }
			        if (sourcesSelector[i].querySelector('option[value$=".flv"]')) {
                        sourcesSelector[i].querySelector('option[value*=".flv"]').disabled = true;
        		    }
                }
            }

            document.addEventListener('DOMContentLoaded', function() {
                var mediaElements = document.querySelectorAll('video, audio'), total = mediaElements.length;

                for (var i = 0; i < total; i++) {
                    new MediaElementPlayer(mediaElements[i], {
                        pluginPath: 'https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.5/',
                        shimScriptAccess: 'always',
                        success: function () {
                            var target = document.body.querySelectorAll('.player'), targetTotal = target.length;
                            for (var j = 0; j < targetTotal; j++) {
                                target[j].style.visibility = 'visible';
                            }
			            }
		            });
                }
            });
    }

 }



 // noinspection JSAnnotator
var content= {
    init :function () {

        content.getComments();
        content.favorite();

    }, sendAjaxPostRequest:function (data) {
         var csrftoken = login.getCookie('csrftoken');

         $.ajax({
             type: "POST",
             dataType: "json",
             url: "/content/add_comment/",
             data:
                 {
                     "comment": data.comment,
                     "contentId":data.contentId
                 },
             headers: {
                 "HTTP_X_CSRF_TOKEN": csrftoken,
                 "X-CSRFToken": csrftoken
             },
             success: function (data) {
                 if (data.response.success == true) {
                     content.getComments();
                 } else {
                     alert('Yorum yapma basarisiz')
                 }
             },
             error: function (XMLHttpRequest, textStatus, errorThrown) {
                 alert("some error " + String(errorThrown) + String(textStatus) + String(XMLHttpRequest.responseText));
             }
         });
     },
     getComments:function () {
          $.ajax({
             type: "GET",
              dataType: "html",
             url: "/content/get_comments/",
             data:
                 {
                     "contentId":window.location.href.split(',')[1]
                 },
             success: function (data) {
                 $('.js-eskimeyen-commentsloader').html(data)


                   $(".js-eskimeyen-comment-send").on('click',function () {

                        var comment = $('.eskimeyen-comment-area').val();

                        if(comment==null || comment=='' )
                            return alert('Yorum alani bos birakilamaz')


                        var url = window.location.href.split(',');
                        var data = {"comment":comment, 'contentId':url[1]}

                        content.sendAjaxPostRequest(data)

                    })

             },
             error: function (XMLHttpRequest, textStatus, errorThrown) {
                 alert("some error " + String(errorThrown) + String(textStatus) + String(XMLHttpRequest.responseText));
             }
         });
     },
    favorite:function () {
         $(".js-favorite-content").click(function () {

             var isEmpty = $(this).hasClass('glyphicon-heart-empty');

             if(isEmpty){
                 $(this).removeClass('glyphicon-heart-empty').addClass('glyphicon-heart');
             }
             else {
                 $(this).removeClass('glyphicon-heart').addClass('glyphicon-heart-empty');
             }
         })

     }
     
 }

video.init()

login.init()

register.init()

content.init()

home.init();
