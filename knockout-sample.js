        $(function () {

            var bindPlugins = function () {

                var $select = $('.custom-select2').select2();

                $select.each(function (i, item) {
                    $(item).select2("destroy");
                });

                $select.select2({ width: 'auto' });
            }

            var occupancyModel = function (data) {

                var self = this;
                self.Id = ko.observable(data.Id)
                self.Name = ko.observable(data.Name);
                //self.AccommodationOptionTypeId(data.AccommodationOptionTypeId)
                self.Items = ko.observableArray([]);

                $.each(data.Items, function (key, value) {

                    self.Items.push(new ItemsModel(value));

                });

                self.AddItems = function () {
                    self.Items.push(new ItemsModel(null));
                    bindPlugins();
                }
                self.RemoveItems = function (occupancy) {

                    self.Items.remove(occupancy);
                }
                self.Save = function () {

                   // console.log($.parseJSON(ko.toJSON(self)));
                    ajaxMethods.sendPostRequest({
                        controller: "Cruise",
                        action: "OccupancyAddOrUpdate",
                        value: { CruiseOccupancy: $.parseJSON(ko.toJSON(self)) },
                        notification: true,
                        block: {
                            element: '#occupancyContainer'
                        },
                        onComplete: function (result) {
                            if (result.Success) {

                                utility.closeModal();
                                $('#CruiseOccupancyListLoader').trigger('reload');
                            }
                        },
                    });

                }
            };


            var ItemsModel = function (data) {
                var self = this;
                data = data || {
                   // Id:0,
                    CruiseOccupancyId: 0,
                    AdultCount: 0,
                    ChildCount: 0,
                    InfantCount: 0,
                    ExtrabedCount: 0,
                    AccommodationOptionTypeId:0
                }
                //self.Id = data.Id
                self.CruiseOccupancyId = ko.observable(data.CruiseOccupancyId);
                self.AdultCount = ko.observable(data.AdultCount);
                self.ChildCount = ko.observable(data.ChildCount);
                self.InfantCount = ko.observable(data.InfantCount);
                self.ExtrabedCount = ko.observable(data.ExtrabedCount);
                self.AccommodationOptionTypeId = ko.observable(data.AccommodationOptionTypeId);

                console.log(self.AccommodationOptionTypeId());

            };

            ko.cleanNode($('#occupancyContainer')[0]);
            ko.applyBindings(new occupancyModel(@Html.Raw(Json.Encode(Model.Occupancy))), document.getElementById('occupancyContainer'));
            bindPlugins();
        });