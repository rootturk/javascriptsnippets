
//Basit bir maliyet/satis hesaplama kodu..


var packageModule = {
    init: function () {
        $('.js-btnCalculate').on('click', function (e) {
            e.stopImmediatePropagation();
            var el = $(this);
            var calculateElements = {
                Rate: ".js-PackageRate",
                DoubleSale: ".js-DoubleSale",
                SingleSale: ".js-SingleSale",
                Child1Sale: ".js-Child1Sale",
                Child2Sale: ".js-Child2Sale",
                Child3Sale: ".js-Child3Sale",
                ExtraBedSale: ".js-ExtraBedSale",
                DoubleCost: ".js-DoubleCost",
                SingleCost: ".js-SingleCost",
                ExtraBedCost: ".js-ExtraBedCost",
                Child1Cost: ".js-Child1Cost",
                Child2Cost: ".js-Child2Cost",
                Child3Cost: ".js-Child3Cost"
            }

            packageModule.calculatePrice($(this), calculateElements);


        });

        $('.js-btnCopyCalculate').on('click', function (e) {
            e.stopImmediatePropagation();
            var el = $(this);
            var calculateElements = {
                Rate: ".js-copyPackageRate",
                DoubleSale: ".js-copyDoubleSale",
                SingleSale: ".js-copySingleSale",
                Child1Sale: ".js-copyChild1Sale",
                Child2Sale: ".js-copyChild2Sale",
                Child3Sale: ".js-copyChild3Sale",
                ExtraBedSale: ".js-copyExtraBedSale",
                DoubleCost: ".js-copyDoubleCost",
                SingleCost: ".js-copySingleCost",
                ExtraBedCost: ".js-copyExtraBedCost",
                Child1Cost: ".js-copyChild1Cost",
                Child2Cost: ".js-copyChild2Cost",
                Child3Cost: ".js-copyChild3Cost"
            }

            packageModule.calculatePrice($(this), calculateElements);


        });

        $('.js-btnlistCalculate').on('click', function (e) {
            e.stopImmediatePropagation();

            var el = $(this);
            var calculateElements = {
                Rate: ".js-listPackageRate",
                DoubleSale: ".js-listDoubleSale",
                SingleSale: ".js-listSingleSale",
                Child1Sale: ".js-listChild1Sale",
                Child2Sale: ".js-listChild2Sale",
                Child3Sale: ".js-listChild3Sale",
                ExtraBedSale: ".js-listExtraBedSale",
                DoubleCost: ".js-listDoubleCost",
                SingleCost: ".js-listSingleCost",
                ExtraBedCost: ".js-listExtraBedCost",
                Child1Cost: ".js-listChild1Cost",
                Child2Cost: ".js-listChild2Cost",
                Child3Cost: ".js-listChild3Cost"
            }


            packageModule.calculatePrice($(this), calculateElements);


        });
        $('.js-btnClearCalculate').on('click', function () {

            var calculateElements = {
                Rate: ".js-copyPackageRate",
                DoubleSale: ".js-copyDoubleSale",
                SingleSale: ".js-copySingleSale",
                Child1Sale: ".js-copyChild1Sale",
                Child2Sale: ".js-copyChild2Sale",
                Child3Sale: ".js-copyChild3Sale",
                ExtraBedSale: ".js-copyExtraBedSale",
                DoubleCost: ".js-copyDoubleCost",
                SingleCost: ".js-copySingleCost",
                ExtraBedCost: ".js-copyExtraBedCost",
                Child1Cost: ".js-copyChild1Cost",
                Child2Cost: ".js-copyChild2Cost",
                Child3Cost: ".js-copyChild3Cost"
            }

            packageModule.clearPrice(calculateElements);

        });



    calculatePrice: function (element, calculateElements) {
        var el = element;

        //Rate : oran
        var packageRate = $(calculateElements.Rate);
        if (packageRate != undefined && packageRate != null && packageRate.val() > 0) {

            //Getter Setter
            var doubleSale = $(calculateElements.DoubleSale);
            var singleSale = $(calculateElements.SingleSale);
            var tripleSale = $(calculateElements.ExtraBedSale);
            var child1Sale1 = $(calculateElements.Child1Sale);
            var child2Sale = $(calculateElements.Child2Sale);
            var child3Sale = $(calculateElements.Child3Sale);

            var doubleCost = $(calculateElements.DoubleCost);
            var singleCost = $(calculateElements.SingleCost);
            var tripleCost = $(calculateElements.ExtraBedCost);
            var child1Cost1 = $(calculateElements.Child1Cost);
            var child2Cost = $(calculateElements.Child2Cost);
            var child3Cost = $(calculateElements.Child3Cost);

            //Calculate Sale Prices
            if (doubleSale != null && doubleSale.val() > 0) {
                doubleCost.val(packageModule.calculateSalePrice(packageRate, doubleSale));
            } else if (doubleCost != null && doubleCost.val() > 0) {
                doubleSale.val(packageModule.calculateCostPrice(packageRate, doubleCost));
            }
            if (singleSale != null && singleSale.val() > 0) {
                singleCost.val(packageModule.calculateSalePrice(packageRate, singleSale));
            } else
                if (singleCost != null && singleCost.val() > 0) {
                    singleSale.val(packageModule.calculateCostPrice(packageRate, singleCost));
                }
            if (tripleSale != null && tripleSale.val() > 0) {
                tripleCost.val(packageModule.calculateSalePrice(packageRate, tripleSale));
            } else if (tripleCost != null && tripleCost.val() > 0) {
                tripleSale.val(packageModule.calculateCostPrice(packageRate, tripleCost));
            }

            if (child1Sale1 != null && child1Sale1.val() > 0) {
                child1Cost1.val(packageModule.calculateSalePrice(packageRate, child1Sale1));
            } else if (child1Cost1 != null && child1Cost1.val() > 0) {
                child1Sale1.val(packageModule.calculateCostPrice(packageRate, child1Cost1));
            }
            if (child2Sale != null && child2Sale.val() > 0) {
                child2Cost.val(packageModule.calculateSalePrice(packageRate, child2Sale));
            } else if (child2Cost != null && child2Cost.val() > 0) {
                child2Sale.val(packageModule.calculateCostPrice(packageRate, child2Cost));
            }
            if (child3Sale != null && child3Sale.val() > 0) {
                child3Cost.val(packageModule.calculateSalePrice(packageRate, child3Sale));
            } else if (child3Cost != null && child3Cost.val() > 0) {
                child3Sale.val(packageModule.calculateCostPrice(packageRate, child3Cost));
            }
            //Calculate Sale Price
            //Calculate Sale Price


        }
        else {

            console.log("Oran değeri giriniz....");
        }


    },
    calculateSalePrice: function (rate, sale) {
        var result = 0;
        if (rate.val() > 0) {
            result = parseFloat(sale.val().replace(/\+/g, ".")) - (parseFloat(sale.val().replace(/\+/g, ".")) * (parseFloat(rate.val().replace(/\+/g, ".")) / 100));
        }
        return result.toFixed(2);
    },
    calculateCostPrice: function (rate, cost) {
        var result = 0;
        if (rate.val() > 0) {
            result = parseFloat(cost.val().replace(/\+/g, ".")) * (parseFloat(rate.val().replace(/\+/g, ".")) / 100 + 1);
        }
        return result.toFixed(2);
    },
    clearPrice: function (calculateElements) {
        //Calculate Sale Prices
        $(calculateElements.DoubleSale).val("");
        $(calculateElements.SingleSale).val("");
        $(calculateElements.TripleSale).val("");
        $(calculateElements.Child1Sale).val("");
        $(calculateElements.Child2Sale).val("");
        $(calculateElements.Child3Sale).val("");
        $(calculateElements.ExtraBedSale).val("");
        $(calculateElements.DoubleCost).val("");
        $(calculateElements.SingleCost).val("");
        $(calculateElements.TripleCost).val("");
        $(calculateElements.ExtraBedCost).val("");
        $(calculateElements.Child1Cost).val("");
        $(calculateElements.Child2Cost).val("");
        $(calculateElements.Child3Cost).val("");
        $(calculateElements.Rate).val("");

        //Calculate Sale Price
    },

}
